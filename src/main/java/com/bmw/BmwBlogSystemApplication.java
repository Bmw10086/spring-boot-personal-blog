package com.bmw;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BmwBlogSystemApplication {

    public static void main(String[] args) {
        SpringApplication.run(BmwBlogSystemApplication.class, args);
    }

}
